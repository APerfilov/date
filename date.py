
def date_function(date_1, date_2):
    """
    Функция вычисляющая разницу между двумя датами в
    формате dd.mm.yyyy.
    """

    leap_year_list = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    normal_year_list = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    # преобразование дат в список
    def __date_handler(date):

        result = date.split('.')

        if int(result[0]) == 0:

            raise Exception(f'Дата {date} введена не корректно! День не может быть равен 0!')
        
        if int(result[1]) == 0:

            raise Exception(f'Месяц {date} введен не корректно! Номер месяца не может быть равен 0!')

        if int(result[1]) > 12:
            raise Exception(f'Месяц {date} введен не корректно! Номер месяца не может быть больше 12!')

        return date.split('.')

    date_1 = __date_handler(date=date_1)
    date_2 = __date_handler(date=date_2)

   # Определение високосный год или нет
    def __leap_year(year):

        leap_flag = False

        if year % 4 == 0:

            leap_flag = True

            if year % 100 == 0:

                if year % 400 == 0:

                    return leap_flag

                else: 
                    return False

            else:
                return leap_flag          

        else:
            return leap_flag

    # Подсчёт количества високосных лет от 1582 года 
    # (дата ввода понятия "високосный год" в обращение)
    def __leap_year_num(date):

        result_list = []

        for i in range(0, int(date[2])):

            if i >= 1582:

                result = __leap_year(year=i)

                if result:

                    result_list.append(i)

        return result_list

    # Подсчёт количества дней по годам
    def __days_in_years(date):

        result = int(date[2]) * 365 + len(__leap_year_num(date))

        return result

    # Подсчёт количества дней по месяцам с учётом високосного года
    def __days_in_month(date):

        result = 0

        if __leap_year(int(date[2])):

            for i in range(0, int(date[1]) - 1):

                result = result + leap_year_list[i]

            return result
        
        else:

            for i in range(0, int(date[1]) - 1):

                result = result + normal_year_list[i]
            
            return result

    # Функция осуществляет проверку корректности введённых данных.
    # Если данные не корректны, то функция выводит предупреждение об ошибке.
    def __days(date):

        if __leap_year(year=int(date[2])):

            if int(date[0]) > leap_year_list[int(date[1]) - 1]:

                raise Exception('Ошибка! В этом месяце не может быть столько дней!')
            
            else:

                return int(date[0])

        else:

            if int(date[0]) > normal_year_list[int(date[1]) - 1]:

                raise Exception('Ошибка! В этом месяце не может быть столько дней!')

            else:
                return int(date[0])



    # Подсчёт количества дней с учётом лет, месяцев и дней в дате.
    def __sum_days(date):

        years = __days_in_years(date=date)
        month = __days_in_month(date=date)
        days = __days(date=date)

        return days + month + years

    # Подсчёт разности
    return abs(__sum_days(date_1) - __sum_days(date_2))
